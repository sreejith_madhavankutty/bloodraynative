package com.example.staneyxavier.androidapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Athira Surendran on 6/24/2015.
 */
public class RegisterActivity extends MainActivity {

    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);


       button = (Button) findViewById(R.id.btnRegister);
        button.setOnClickListener(new View.OnClickListener() {



            @Override

            public void onClick(View arg0) {

                Toast.makeText(getApplicationContext(), "Your Account is created", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(RegisterActivity.this, HomeScreen.class);
                startActivity(intent);

            }

        });

    }
}
